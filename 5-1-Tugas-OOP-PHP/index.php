<?php

class hewan{
    public $nama;
    public $darah;
    public $jumlahkaki;
    public $keahlian;
    public $atpower;
    public $defpower;
    
    public function __construct($nama, $darah, $jumlahkaki, $keahlian, $atpower, $defpower){
        $this->nama = $nama;
        $this->darah = $darah;
        $this->jumlahkaki = $jumlahkaki;
        $this->keahlian = $keahlian;
        $this->atpower = $atpower;
        $this->defpower = $defpower;
    }

    public function tampilkan(){
        return 
        "Nama hewan : " . $this->nama . "<br>" . 
        "Darah : " . $this->darah . "<br>" .
        "Jumlah kaki : " . $this->jumlahkaki . "<br>" .
        "Keahlian : " . $this->keahlian . "<br>" .
        "Attack power : " . $this->atpower . "<br>" .
        "Deffence power : " . $this->defpower . "<br>" . "<br>";

    }
    public function atraksi($nama , $keahlian){
        return $this->nama . "sedang" . $this->keahlian;
}

}
$elang = new hewan('elang' , 50 , 2 , 'terbang tinggi' , 10 , 5);
echo $elang->tampilkan();

$harimau = new hewan('harimau' , 50 , 4 , 'lari cepat' , 7 , 8);
echo $harimau->tampilkan(); 

class Elang 
{
    public static function atraksi()
    {
        echo 'Elang sedang terbang tinggi' . '<br>' ;
    }
}

class Fight 
{
    public static function serang()
    {
        echo 'Harimau sedang menyerang elang';
    }
}


echo Elang::atraksi();
echo Fight::serang();


?>